""" Functions for reading .dat image sequences from the HPV1 camera

"""
import binascii
from numpy import rot90,reshape, dstack
from struct import unpack

def hpv1dat_to_np(hpv1dat):
    """ return a 260 x 312 x 102 array of image data """
    keys = [int(key) for key in hpv1dat.keys()]
    keys = sorted(keys)
    imlist = []
    for key in keys:
        imlist.append(hpv1dat[str(key)])
    return dstack(imlist)

def load_hpv1dat(filename,imrot = 0,with_header=False):
    """
    Load a hpv1 .dat file from the shimadzu camera
    
    INPUT:
    filename
    imrot - how many 90 degree clockwise rotations to perform (default don't rotate)
    with_header - if you want the header set to True (not sure it actually works...)
      seems like timestamp is (Y,M,?,D,H,M,S,ms)
                 rec_speed is {1Mhz: 0
                               250kHz: 2
                               32khz: 5}
    """
    header = {}
    imdat = {}
    with open(filename,'rb') as fid:
        if with_header:
            header['timestamp'] = unpack('H'*8,fid.read(16))
            header['record_version'] = unpack('i',fid.read(4))[0]
            header['display_mode'] = unpack('i',fid.read(4))[0]
            header['comment'] = [binascii.b2a_uu(e) for e in unpack('c'*260,fid.read(260))]
            header['play_speed'] = unpack('i',fid.read(4))[0]
            header['frame_number'] = unpack('i',fid.read(4))[0]
            header['start_frame'] = unpack('i',fid.read(4))[0]
            header['stop_frame'] = unpack('i',fid.read(4))[0]
            header['magnification'] = unpack('i',fid.read(4))[0]
            header['rotation'] = unpack('i',fid.read(4))[0]
            header['brightness'] = unpack('i',fid.read(4))[0]
            header['contrast'] = unpack('i',fid.read(4))[0]
            header['AD1'] = unpack('i',fid.read(4))[0]
            header['AD2'] = unpack('i',fid.read(4))[0]
            header['AD3'] = unpack('i',fid.read(4))[0]
            header['AD4'] = unpack('i',fid.read(4))[0]
            header['AD5'] = unpack('i',fid.read(4))[0]
            header['rec_mode'] = unpack('i',fid.read(4))[0]
            header['rec_speed'] = unpack('i',fid.read(4))[0]
            header['rec_exposure'] = unpack('i',fid.read(4))[0]
            header['monitor_exposure'] = unpack('i',fid.read(4))[0]
            header['trig_polarity'] = unpack('i',fid.read(4))[0]
            header['trig_delay'] = unpack('i',fid.read(4))[0]
            header['trig_point'] = unpack('i',fid.read(4))[0]
            header['prgm_speed'] = unpack('i',fid.read(4))[0]
            header['prgm_point'] = unpack('i',fid.read(4))[0]
            header['illum_type'] = unpack('i',fid.read(4))[0]
            header['illum_timing'] = unpack('i',fid.read(4))[0]
            header['illum_start'] = unpack('i',fid.read(4))[0]
            header['illum_stop'] = unpack('i',fid.read(4))[0]
            header['set_cool_temp'] = unpack('i',fid.read(4))[0]
            header['current_temp'] = unpack('i',fid.read(4))[0]
            header['cool_mode'] = unpack('i',fid.read(4))[0]
            header['synch_mode'] = unpack('i',fid.read(4))[0]
            header['disp_frame_no'] = unpack('i',fid.read(4))[0]
            header['AD6'] = unpack('i',fid.read(4))[0]
            header['AD7'] = unpack('i',fid.read(4))[0]
            header['AD8'] = unpack('i',fid.read(4))[0]
            header['AD9'] = unpack('i',fid.read(4))[0]
            header['AD10'] = unpack('i',fid.read(4))[0]
        else:
            fid.read(428)
        for i in range(102):
            imdat[str(i)] = reshape(unpack('H'*312*260,fid.read(162240)),(260,312))
            if imrot > 0:
                imdat[str(i)] = rot90(imdat[str(i)],imrot)
        if with_header:
            return hpv1dat_to_np(imdat),header
        return hpv1dat_to_np(imdat)