""" Filters to go here

"""
from scipy.ndimage import measurements


def histogram_clip(im, low=0, high=1):
    """ Calculate the histogram and set all values less than the low*100 % th 
    intensity to zero and the values high*100%th to the max value """
    im_output = im.copy()
    bit_depth = 2**16
    min_val = 0
    max_val = bit_depth-1
    low_percent = int(low*im.size)
    high_percent = int((1-high)*im.size)
    hist = measurements.histogram(im, min_val, max_val, bit_depth)
    sumf = sumr = 0
    low_flag = high_flag = False
    for i in range(len(hist)):
        sumf += hist[i]
        sumr += hist[- 1 - i]
        if sumf >= low_percent and not low_flag:
            low_flag = True
            low_thresh = i
        if sumr >= high_percent and not high_flag:
            high_flag = True
            high_thresh = len(hist) - i
        if high_flag and low_flag:
            break
    im_output[im < low_thresh] = low_thresh
    im_output[im > high_thresh] = high_thresh
    return im