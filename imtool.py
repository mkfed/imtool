""" Imtool main python file

"""
from numpy import array,meshgrid,arange
from struct import unpack
from scipy.misc import imsave,imread
from warnings import warn
from os import listdir, path

from b16lib import *
from b16datlib import *
from hpv1lib import *
from filters import *
#import process

########### OTHER FUNCTIONS ############
def meshgrid_image(im_shape):
    """ Returns an x coordinate and y coordinate array of pixel locations

    INPUT:
      im_shape => im.shape tuple (x,y)
    OUTPUT:
      xx,yy =>np.meshgrid(x list,y list)
    """
    x=arange(0,im_shape[0])
    y=arange(0,im_shape[1])
    return meshgrid(y,x) 
    
def load_pgm(filename):
    """ Loads a file from binary pgm format into a numpy array

    [*] Pixel intensities are 8 bit unsigned integers.
        In struct.unpack this is unsigned char, 1 byte(8 bit)/pixel  => 'B'
    
    INPUT:
      filename => name of file to import
    OUTPUT:
      numpy array of dtype='uint8' of the image
    """
    with open(filename,'rb') as fid:
        raw_data=fid.read(3)
        if raw_data != 'P5 ':
            return "I wasn't designed for this!! File must be PGM binary"
        header_details=''
        raw_data=fid.read(1)
        while raw_data !='\n':
            header_details +=raw_data
            raw_data=fid.read(1)
        width,height,max_intensity = header_details.split(' ')
        width=int(width)
        datatype='uint8'
        if max_intensity !='255':
            print('Ohh No, I broke because the maximum intensity was not 255.')
            print('Max intensity: ' + max_intensity)
            raise
        image_data = []
        raw_data = fid.read(width)
        while raw_data:
            image_data.append(unpack('B'*width,raw_data))
            raw_data = fid.read(width)
    return array(image_data,dtype=datatype)
    
############### DEAL WITH OPENING AND SAVING IMAGES ########
def load(filename):
    """ Load image to numpy array based on file extension.

    [*] Calls internal functions if .b16 or .pgm file is loaded and uses scipy.misc for others.
    INPUT:
      filename => filename including directory if not in cwd
    OUTPUT:
      numpy array of image data or False if loading failed
    """
    if len(filename)<5:
        return False
    if filename[-4:] == '.b16':
        return load_b16(filename)
    if filename[-4:] == '.Dat':
        return load_hpv1dat(filename)
        
    try:
        return imread(filename, flatten=True) 
    except:
        if filename[-4:] == '.pgm':
            return load_pgm(filename)
        raise

def lsim(filename,ftype='png'):
    """Loads and saves an image to file type (default is png)

    INPUT:
      filename => image to be saved
    """
    
    im=load(filename)
    try:
        save(filename[:filename.rfind('.')+1]+ftype,im)
        return True
    except:
        print('FAILED TO SAVE %s'%filename)
        return False

def save(filename,im,ftype=''):
    """ Saves an image file of im as the type given in the extension of filename or in ftype

    INPUT:
      filename => Name of file (with directory if relevent)
      im       => numpy array of image to save
      ftype    => file type if not included in filename
    OUTPUT:
      saves file, returns None
    """
    if filename.rfind('.')==-1:
        if ftype.find('.')==-1 and len(ftype)>2:
            filename+='.'+ftype
        elif ftype.find('.')==-1:
            raise TypeError("File type not recognised")
        else:
            filename+=ftype
    else:
        if ftype and filename[-len(ftype):]!=ftype:
            warn("Given file type doesn't match filename. Continuing based on filename only")
        ftype = filename[filename.rfind('.'):]
    if ftype == '.b16':
        write_b16(filename, im)
        return
    imsave(filename,im)
    
    
############### HANDLE BULK IMAGES ###########
def bulk_convert_images(folder,from_type,to_type):
    """ Convert all image files in folder of from_type to to_type

    INPUT:
      folder    => directory to folder
      from_type => type of image file to convert from, eg. 'b16'
      to_type   => type of image file to convert to, eg. 'png'
    OUTPUT:
      saves files, returns None
    """
    def remove_leading_dot(s):
        if s[0] == '.':
            return s[1:]
        return s
    from_type = remove_leading_dot(from_type)
    to_type = remove_leading_dot(to_type)
    files = listdir(folder)
    print('Found {} files'.format(len(files)))
    for f in files:
        if f.find('.'+from_type)>=0:
            lsim(path.join(folder, f),to_type)
            print(f)
    return