from numpy import array,reshape
from struct import unpack,pack
from scipy.misc import imread
import os
import numpy as np
from warnings import warn
from b16lib import series_info

""" Functions for reading and writing .B16dat image files

"""

class B16dat_reader():
    def __init__(self, fname):
        """ Initialise the b16dat object with the image file """
        self.fname = self.check_fname(fname)
        self.f = open(self.fname, 'rb')
        dat = self.f.read(4)
        self.height,self.width = unpack('HH',dat)
        self.file_size = os.path.getsize(self.fname)
        self.fsize = os.stat(self.fname).st_size
        # subtract 4 because the first two shorts are width, height and short is 2 bytes per number
        self.num_files = int((self.fsize - 4) / self.width / self.height / 2)
        if self.num_files != (self.fsize - 4) / self.width / self.height / 2:
            warn('Number of images in file is a non-integer. This could indicate: 1. Wrong height or width, 2. Incomplete write of image, 3. Problem with appending to file')
    def __str__(self):
        str_out = """B16dat_reader object of {fname}.
        Number of images: {nim}
        Image dimensions: {height} x {width}
        """.format(fname = self.fname, 
                   nim = self.num_files, 
                   height = self.height, 
                   width = self.width)
        return str_out     
        
    def __iter__(self):
        return self
    
    def __next__(self):
        return self.read_next_image()

    def __getitem__(self, num):
        return self.read_image(num)

    def skip_next(self):
        self.f.seek(self.width * self.height * 2, 1)
    
    def check_fname(self, fname):
        if os.path.isfile(fname):
            return fname
        if os.path.isfile(fname + '.B16dat'):
            return fname + '.B16dat'
        if os.path.isfile(fname + '.b16dat'):
            return fname + '.b16dat'
        raise FileNotFoundError('Cannot find the file: {}'.format(fname))
        
    def read_image(self, num):
        if num > self.num_files:
            raise IndexError('Trying to access image {} but {} only contains {} files.'.format(num, self.fname, self.num_files))
        current_pos = self.f.tell() #record current position to return to it later
        self.f.seek(4 + self.width * self.height * 2 * num, 0)
        im = self.read_next_image()
        self.f.seek(current_pos)
        return im
    
    def read_next_image(self):
        dat = self.f.read(self.width * self.height * 2)
        if dat:
            dat = unpack('H'* self.width * self.height, dat)
            return reshape(array(dat), (self.width,self.height))
        self.close()
        raise StopIteration
        
    def close(self):
        self.f.close()
        
class B16dat_writer():
    def __init__(self, fname, split_double_shutter = False, append=False):
        print('Writing B16dat file...', flush=True)
        if split_double_shutter:
            print('Splitting double shutter images', flush=True)
        if append:
            self.f = open(fname,'r+b')
            ## act like a B16dat_reader
            dat = self.f.read(4)
            self.height,self.width = unpack('HH',dat)
            self.file_size = os.path.getsize(fname)
            self.fsize = os.stat(fname).st_size
            self.num_files = (self.fsize - 4) / self.width / self.height / 2
            self.f.seek(0,2) #seek to end of the file for appending
            self.first = False
        else:
            self.f = open(fname,'wb')
            self.first = True
        self.split_double_shutter = split_double_shutter
        self.imcount = 0
    
    def add_image(self, im):
        if type(im) is str: #load image directly from file
            print('Writing {} to file.'.format(im),flush=True)
            fid = open(im,'rb')
            raw_data = fid.read(20)
            file_size,header_size = unpack('ii',raw_data[4:12])
            width, height = unpack('ii',raw_data[12:20])
            fid.read(header_size-20)
        else: #assume numpy array
            height, width = im.shape
        if self.split_double_shutter:
            if int(height/2) != height / 2:
                raise ValueError('Odd number height can\'t be interpreted as doubleshutter image')
            height = height // 2
        if self.first:
            print('Images dimensions: height {}px x width {}px'.format(height,width), flush=True)
            self.height = height
            self.width  = width
            self.f.write(pack('HH', self.width, self.height))
            self.first = False
        if height != self.height and width != self.width:
            raise Exception('Image size not consistent')
        if type(im) is str:
            self.f.write(fid.read())
            fid.close()
        else:
            for row in im:
                self.f.write(pack('H'*width,*row))     
        self.f.flush()
        if self.split_double_shutter:
            self.imcount += 2
        else:
            self.imcount += 1
            
    def close(self):
        print('{} images written to file ({} files found)'.format(self.imcount, self.imcount // (int(self.split_double_shutter)+1)),flush=True)
        self.f.close()

def load_im_from_b16dat(fname, imnum = 0):
    imseq = B16dat_reader(fname)
    im = imseq.read_image(imnum)
    imseq.close()
    return im

def read_b16dat(fname):
    all_ims = []
    for im in B16dat_reader(fname):
        all_ims.append(im)
    return all_ims
    
def extract_b16dat(b16datfile, b16datout, first_image = 0, num_to_extract = 1, list_images_to_extract = None):
    #clarify whether counting starts from 0 or 1. At the moment it starts from 0

    a = B16dat_reader(b16datfile)
    b = B16dat_writer(b16datout)
    
    if list_images_to_extract:
        for i in list_images_to_extract:
            b.add_image(a.read_image(i))
        return
    
    for i in range(first_image):
        a.skip_next()
    for i in range(num_to_extract):
        b.add_image(a.read_next_image())
    b.close()
    a.close()
        
def split_b16dat(b16datfile, num_images=250,first_image = 0, carry_on = False):
    MAX_FILES = 50 #until I figure out a better way to implement this, this will stop infinite looping
    if first_image != 0:
        print('First image is not zero so if this fails you can\'t use the carry_on option to restart it')
    a = B16dat_reader(b16datfile)
    fname, ext = os.path.splitext(b16datfile)
    i=0
    print('splitting b16 dat into files with num images = {}'.format(num_images), flush=True)
    counter = 0
    if type(num_images) is list:
        max_im = num_images[counter]
    else:
        max_im = num_images
    while carry_on:
        if carry_on:
            if os.path.isfile(fname + '-{}'.format(counter) + ext):
                counter += 1
            else:
                counter -= 1
                break
            if counter > MAX_FILES:
                print('File counter exceeded the maximum for splitting of {}. This is hardcoded and can easily be changed in the source code.'.format(MAX_FILES))
                return
            continue
    if carry_on:
        print('\n-- test opening, not expecting to write anything --')
        b = B16dat_writer(fname + '-{}'.format(counter) + ext, append = True)
        print('-- --------------------------------------------- --\n')
        if b.num_files < num_images:
            raise Exception('This case hasnn\'t been properly tested yet. Delete the most recent not full file and it will work')
            pos = b.f.tell() #this includes the 4  bytes for image size...so when seeking in the next line you have no extra offset  of 4 bytes
            a.f.seek(a.height * a.width * 2 * counter*num_images + pos ,0)
            i = b.num_files
        else:
            counter += 1
            b.close()
            b = B16dat_writer(fname + '-{}'.format(counter) + ext)
            a.f.seek(4 + a.height * a.width * 2 * counter*num_images ,0)
    else:
        b = B16dat_writer(fname + '-{}'.format(counter) + ext)
    for im in a:
        b.add_image(im)
        i += 1
        if i == max_im:
            b.close()
            print('File {} done'.format(fname + '-{}'.format(counter) + ext), flush=True)
            counter += 1
            b = B16dat_writer(fname + '-{}'.format(counter) + ext)
            i=0
            if type(num_images) is list and counter < len(num_images)-1:
                max_im = num_images[counter]
        elif i > max_im:
            raise ValueError('i can\'t possibly be larger than max_im unless something went horribly wrong with carry_on=True')
    b.close()
    print('Final file contains {} images'.format(i))
    print('File {} done'.format(fname + '-{}'.format(counter) + ext), flush=True)

def b16dat_mean(b16dat_image):
    updatenum = 250
    print('Calculating mean image of {}. Giving an update every {} images'.format(b16dat_image, updatenum), flush = True)
    f = B16dat_reader(b16dat_image)
    avg_im = f.read_next_image().astype(np.uint32)
    i = 1
    for im in f:
        avg_im += im.astype(np.uint32)
        i += 1
        if i % updatenum == 0:
            print(i,flush = True)
        if i > 32767:
            print('The mean image is possibly using bad data because it is using a data type np.uint32 and numbers might now be getting too big for it')
    avg_im = avg_im/i
    fname, ext = os.path.splitext(b16dat_image)
    fname += '_avg' + ext
    imout = B16dat_writer(fname)
    imout.add_image(avg_im.astype(np.uint32))
    print('Average image written to {}'.format(fname))
    imout.close()
    print('Average image generation complete.', flush = True)

def b16dat_mean_doubleshutter(b16dat_image):
    updatenum = 250
    print('Calculating mean image of {}. Giving an update every {} images'.format(b16dat_image, updatenum), flush = True)
    f = B16dat_reader(b16dat_image)
    avg_im1 = f.read_next_image().astype(np.int32)
    avg_im2 = f.read_next_image().astype(np.int32)
    
    i = 2
    for im in f:
        i += 1
        if i%2 == 0:
            avg_im1 += im.astype(np.int32)
        if i%2 == 1:
            avg_im2 += im.astype(np.int32)

        if i % updatenum==0:
            print(i,flush=True)
        if i > 32767 * 2:
            print('Assuming 16bit images, the mean image is now possibly using bad data because it is using a data type int32 and numbers might now be getting too big for it')
    avg_im1 = avg_im1 / (i / 2)
    avg_im2 = avg_im2 / (i / 2)
    fname, ext = os.path.splitext(b16dat_image)
    fname += '_avg{}' + ext
    imout = B16dat_writer(fname.format(1))
    imout.add_image(avg_im1.astype(np.int32))
    print('Average image 1 written to {}'.format(fname.format(1)))
    
    imout = B16dat_writer(fname.format(2))
    imout.add_image(avg_im2.astype(np.int32))
    print('Average image 2 written to {}'.format(fname.format(2)))
    
    imout.close()
    print('Average image generation complete.', flush=True)
        
    
def b16dat_mask(b16dat_file, mask, masked_output_file = None, carry_on = False):
    #mask is a binary file where pixels corresponding to 1 are unchanged and 0 are set to 0
    if type(mask) is str:
        mask =  imread(mask, True)
    if not masked_output_file:
        fname, ext = os.path.splitext(b16dat_file)
        masked_img = fname + '_masked' + ext
    mask = mask / mask.max()
    def mask_filter(im):
        return (im * mask).astype(np.int32)
    b16dat_filter(b16dat_file, masked_img, mask_filter, carry_on)
       
       
def b16dat_mask_and_mean(b16dat_file, mask, mean_img = None, generate_mean = False, output_file = None, carry_on = False):     
    print('Subtracting mean and masking the image series.', flush = True)
    if not output_file:
        fname, ext = os.path.splitext(b16dat_file)
        output_file = fname + '_rmmean_masked' + ext
    if generate_mean:
        b16dat_mean(b16dat_file)
    avg_im = read_b16dat(mean_img)[0]
    print('Mean image found {}.'.format(mean_img), flush = True)
    
    if type(mask) is str:
        mask_name = mask
        mask = imread(mask, True)
        print('Mask image found {}.'.format(mask_name), flush = True)

    mask = mask / mask.max()
        
    def avg_filter(im):
        tmp = im - avg_im
        tmp[tmp < 0] = 0
        return tmp
        
    def mask_filter(im):
        return (im * mask).astype(np.int32)
    print('Filters to be applied in order are:\n\t1. Mean subtraction\n\t2. Mask.', flush=True)
    b16dat_filter(b16dat_file, output_file, avg_filter, mask_filter, carry_on)

def b16dat_remove_mean(b16dat_input, mean_img = None, generate_mean = False, carry_on=False):
    fname, ext = os.path.splitext(b16dat_input)
    if generate_mean:
        b16dat_mean(b16dat_input)
    if mean_img is None:
        mean_img = fname + '_avg' + ext
    avg_im = read_b16dat(mean_img)[0]
    
    def avg_filter(im):
        tmp = im - avg_im
        tmp[tmp<0] = 0
        return tmp
    b16dat_filter(b16dat_input, mean_img, avg_filter, carry_on)
    
def b16dat_remove_mean_doubleshutter(b16dat_input):
    fname, ext = os.path.splitext(b16dat_input)
    mean_img = fname + '_avg{}' + ext
    avg_im1 = read_b16dat(mean_img.format(1))[0]
    avg_im2 = read_b16dat(mean_img.format(2))[0]   
    
    def avg_filter(im, avg_im_num):
        if avg_im_num == 1:
            tmp = im - avg_im1
        else:
            tmp = im - avg_im2
        tmp[tmp<0] = 0
        return tmp
    
    f = B16dat_reader(b16dat_input)
    
    imout = B16dat_writer(fname + '_avgrem' + ext)
    count = 0
    update = 250
    for im in f:
        if count % update == 0:
            print(count, flush=True)
        count += 1
        im = avg_filter(im,count%2)
        imout.add_image(im)
    imout.close()
    f.close()
#    b16dat_filter(b16dat_input, mean_img, avg_filter, carry_on)
    
def b16dat_filter(b16dat_input, b16dat_output, *filter_functions, carry_on=False):
    """ the filter functions are just functions that take an image as input, do something to the image and return an image"""
    print('Applying appropriate filters and writing output B16dat file :)', flush=True)
    f = B16dat_reader(b16dat_input)
    print('f has {} files'.format(f.num_files))
    if carry_on:
        raise Exception('This hasn\'t been tested yet')
        imout = B16dat_writer(b16dat_output, append = True) #append=True opens the file with 'r+b' which points to the end of the file for appending
        print('imout has {} files'.format(imout.num_files))
        pos = imout.f.tell()
        f.f.seek(pos)
    else:
        imout = B16dat_writer(b16dat_output)
    if not filter_functions:
        raise IOError('No filter functions were input')
    
    for im in f:
        for fil in filter_functions:
            if not fil:
                continue
            im = fil(im)
        imout.add_image(im)
        
    imout.close()  
    f.close()

def write_b16dat(first_image, out_fname = None, split_double_shutter = False, nimages = np.inf):
    """ Write B16dat file using a series of b16 images
    """
    in_folder,first_image = os.path.split(first_image)
    series_name, num, fext = series_info(first_image)
    if not out_fname:
        out_fname = os.path.join(in_folder, series_name + '.B16dat')
    if fext != '.b16':
        raise TypeError('Expecting b16 images only for this function')
#    f = open(out_fname, 'wb')
    f = B16dat_writer(out_fname, split_double_shutter = split_double_shutter)
    pad_zeros = len(num)
    num = int(num)
    nref = num
    ##new code using B16dat_writer class
    try:
        while True:
            f.add_image('{}_{}{}'.format(os.path.join(in_folder, series_name), str(num).zfill(pad_zeros), fext))    
            num += 1
            if num - nref == nimages:
                break
    except FileNotFoundError:
        f.close()
        print('File not found: {}{}{}'.format(os.path.join(in_folder, series_name), str(num).zfill(pad_zeros), fext))
    except:
        f.close()
        raise