import sys, os
import numpy as np
import imtool
rotate = 0
fname = sys.argv[1]
if os.path.splitext(fname)[-1].lower() == '.shf':
    path, f = os.path.split(fname)
    fout = '{}Dat'.format(f[:-3])
    path = os.path.join(path, fout)
    fname = os.path.join(path, fout)

try:
    im = imtool.load_hpv1dat(fname)
except FileNotFoundError as e:
    print('\nFileNotFoundError')
    print(e)
    input()
    sys.exit()

def generate_imout(imnums, im):
    imout = np.zeros((im.shape[0]*len(imnums), im.shape[1]))
    for j, e in enumerate(imnums):
        imout[im.shape[0]*j : im.shape[0] * (j+1), :] = im[:,:,e]
    return imout

print('''Enter a single number, 
      several numbers separated by a comma or 
      a slice like using numpy arrays...
      
      rotate=n -> number of ccw 90 degree rotations
      
      Type 'q' to exit''')

print('Displaying frame 0\n')
imtool.toimage(im[:,:,0]).show()
while True:
    print('Enter frame number to display')
    i = input()
    if i == 'exit' or i == 'q':
        break
    if 'rotate' in i:
        rotate = int(i.split('=')[-1])
        im = imtool.load_hpv1dat(fname, imrot=rotate)
        print('Loaded rotated images.'.format(rotate))
        continue
    try:
        i = int(i)
        imout = im[:,:,i]
    except KeyboardInterrupt:
        break
    except ValueError:
        try:
            m = i.split(',')
            i = [int(e) for e in m]
            imout = generate_imout(i, im)
        except ValueError:
            try:
                m = i.split(':')
                m = [int(e) for e in m]
                if len(m) == 1:
                    print('Invalid Value\n')
                    continue
                i = [e for e in range(*m)]
                imout = generate_imout(i, im)
                
            except ValueError:
                print('Invalid Value\n')
                continue
            except IndexError:
                print('Invalid Value\n')
                continue
        except IndexError:
            print('Invalid Value\n')
            continue
    except IndexError:
        print('Invalid Value\n')
        continue
        
    print('Frames: {}'.format(i))
    imtool.toimage(imout).show()
        