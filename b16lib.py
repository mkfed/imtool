#!/usr/bin/env python
""" Functions for reading and writing .b16 image files

Some cameras scale the image data in different ways so there are camera 
specific functions. 

[*] Note that the output is a single numpy array if split_doubleshutter = False
    or two numpy arrays if split_doubleshutter = True.
    eg.
      im1, im2 = load_pco4000(filename, split_doubleshutter=True)
      im = load_pco4000(filename, split_doubleshutter=False)

To load images use:
im = load_pco4000(filename, split_doubleshutter=False)
im = load_dimax(filename, split_doubleshutter=False)
im = load_pixelfly(filename, split_doubleshutter=False)
im = load_scmos(filename, split_doubleshutter=False)

for an image not from one of the above cameras and no image scaling use:
im = no_scale_load(filename, split_doubleshutter=False, dtype=float64)

for an image not from one of the above cameras and with image scaling use:
im = scaled_load(filename, scale, split_doubleshutter=False, dtype=float64)

to load the uint16 image data use:
im = load_b16(filename)

to write a b16 file:
write_b16(filename, im)
write_doubleshutter_b16(filename, im1, im2)
"""

from struct import unpack, pack
from numpy import array, zeros, float64, uint16
import re, os

##################################################################
################# Camera specific load functions #################
##################################################################
def load_pco4000(filename, split_doubleshutter=False, output_type=float64):
    scale_factor = 4
    return scaled_load(filename, scale_factor, split_doubleshutter, output_type)

def load_dimax(filename, split_doubleshutter=False, output_type=float64):
    scale_factor = 16
    return scaled_load(filename, scale_factor, split_doubleshutter, output_type)
    
def load_pixelfly(filename, split_doubleshutter=False, output_type=float64):
    return no_scale_load(filename, split_doubleshutter, output_type)

def load_scmos(filename, split_doubleshutter=False, output_type=float64):
    return no_scale_load(filename, split_doubleshutter, output_type)
##################################################################

### b16_header information
"""
PCO-
file size
header size (1024 => no comment, comments add on to the end of the header)
width
height
extended header (should give -1 = true)
colour mode: 0 = b/w 1 = colour
...
"""
def b16_timestamp_to_float(timestamp):
    """ Convert a timestamp list obtained from get_b16_timestamp(im)
    into a floating point value representative of day/hour/minute/second/partsecond
    
    [*] Note: This doesn't take month or year into account """
    if len(timestamp) != 8:
        raise ValueError('Timestamp must be of the format output from get_b16_timestamp(im)')
    tt  = timestamp[7]
    tt += timestamp[6]
    tt += timestamp[5] * 60
    tt += timestamp[4] * 3600
    tt += timestamp[2] * 24 * 3600
    return tt
    
def b16_timestamp_to_string(timestamp):
    """ Convert a timestamp list obtained from get_b16_timestamp(im)
    into a string.
    
    """
    tmp = timestamp.pop()
    timestamp[-1] += tmp
    return 'Image number: {}\nDate: {}-{}-{}\nTime: {}:{}:{}'.format(*timestamp)
    
def scaled_load(filename, scale, split_doubleshutter=False, dtype=float64):
    """ Scale the image when it is loaded (eg. for loading images from PCO.Dimax)
    """
    if split_doubleshutter:
        return [(im / scale).astype(dtype) for im in load_b16_doubleshutter(filename)]
    return (load_b16(filename) / scale).astype(dtype)

def no_scale_load(filename, split_doubleshutter=False, dtype=float64):
    """ Just load the b16 images without scaling
    """
    return scaled_load(filename, 1, split_doubleshutter, dtype)
    

def load_b16(filename):
    """ Loads a file from PCO b16 format into a numpy array
    This function reads the header of the b16 file to determine the header size
    and the image width. It reads the image row by row and stores data in a list.
    The list is converted to a numpy array.
    
    [*] Pixel intensities are stored as 16 bit unsigned integers in b16 file.
        In struct.unpack this is unsigned short => 2 bytes per number => 'H'

    INPUT:
      filename => name of file to import
    OUTPUT:
      numpy array of dtype='uint16' of the image.
    """
    with open(filename, 'rb') as fid:
        raw_data = fid.read(20) #Read first 20 bytes of header
        file_size, header_size = unpack('ii', raw_data[4:12])
        width, height = unpack('ii', raw_data[12:20])
        fid.read(header_size - 20) # Discard the rest of the header
        image_data = [] 
        raw_data = fid.read(width * 2) 
        while raw_data:  
            image_data.append(unpack('H' * width, raw_data))
            raw_data = fid.read(width * 2)
    return array(image_data, dtype=float64)
    
def load_b16_doubleshutter(filename):
    """Load and split doubleshutter B16 image into individual images
    """
    im = load_b16(filename)
    im1 = im[:int(im.shape[0] / 2), :]
    im2 = im[int(im.shape[0] / 2): , :]
    return im1, im2
    
def write_b16(filename, im):
    """ Writes the numpy array im to filename.
    This function writes a short header only containing 'PCO-', file size,
    header size, width, height and no extended header paramater.

    INPUT:
      im       => numpy array to be saved to file
      filename => name of file (include .b16)
    OUTPUT:
      Writes file to folder, returns True
    """
    i = filename.rfind('.')
    if i == -1:
        filename += '.b16'
    else:
        filename = filename[:i] + '.b16'
    im = convert_to_16bit(im)
    pco = 760169296 #Unpacked 'PCO-'
    header_size = 24 #header size if not extended
    file_size = im.size * 2 + header_size #2 bytes (16 bits) per element in im + header
    width = im.shape[1]
    height = im.shape[0]
    extended_header = 0 #0=False, -1 = True => needs to be 1024 bytes long if True
    header = (pco, file_size, header_size, width, height, extended_header)
    header = pack('i' * len(header), *header)
    with open(filename, 'wb') as fid:
        fid.write(header)
        for row in im:
            fid.write(pack('H' * width, *row))
    return True


def write_doubleshutter_b16(filename, im1, im2):
    imout = combine_images_to_doubleshutter(im1, im2)
    return write_b16(filename, imout)
    
def combine_images_to_doubleshutter(im1, im2):
    """ Combine two images of the same shape to form a single doubleshutter image
    INPUT:
      im1, im2 - numpy arrays of the two images
    OUTPUT:
      imout
    """
    imout_shape = (im1.shape[0] * 2, im1.shape[1])
    imout = zeros(imout_shape)
    imout[:im1.shape[0], :] = im1.copy()
    imout[-im1.shape[0]:, :] = im2.copy()
    return imout

def load_binary_b16_timestamp(filename):
    """ Load and unpack (but not interpret) only the binary timestamp from a 
    b16 image given the filename as an input. No image data is loaded.
    
    """
    with open(filename, 'rb') as fid:
        timestamp_length = 16 #pixels
        raw_data = fid.read(20) #Read first 20 bytes of header
        file_size, header_size = unpack('ii', raw_data[4:12])
        fid.read(header_size - 20) # Discard the rest of the header
        bin_timestamp = fid.read(timestamp_length * 2) 
    return [e for e in unpack('H' * timestamp_length, bin_timestamp)]

def convert_to_16bit(im):
    """ force a numpy array to be converted to 16 bit output with a reasonable range of values
    
    INPUT:
      im       => numpy array of an image
    OUTPUT:
      modified numpy image array of dtype uint16
    """
    if im.dtype == uint16:
        return im
    if 'uint' in im.dtype.name:
        return im.astype(uint16)
    if (im < 0).any():
        im = im.astype(float64)
        im -= im.min()
    #Assume here that the data is some kind of floating point data
    if im.max() > 1:
        if ((im - im.astype(uint16)) == 0).all():
            #this is just integers stored as floats
            return im.astype(uint16)
    
    #Assume continuous range of floating point values between 0 and im.max()
    im = im / im.max() * (2**16 - 1)  #scale output to 16 bits
    return im.astype(uint16)
    

def get_b16_timestamp(im, scaled=False):
    """ Needs further testing...worked well with Dimax but not ILA SCMOS
    Gets the binary timestamp from a b16 image (returns junk if
    no timestamp was recorded on the image)
    Timestamp is recorded in the first 16 pixels of the image.
    
    The timestamp is recorded as: 
    image# date time
    Specific pixels are:
    - 0 1 2 3 - 4 5 6 7 - 8 9 10 11 12 13 14 - 15 16
    - image # -  date   -        time        - ?? ??
    
    pixel correspondence:
    image # (eg. 10345)
    0 - million/ten million unit (eg. 0)
    1 - ten thousand/thousand unit (eg. 1)
    2 - hundred/thousand unit (eg. 3)
    3 - one/ten unit (eg. 45)
    date (eg.24 JUN 2014):
    4 - year first 2 digits (eg. 20)
    5 - year second 2 digits(eg. 14)
    6 - month (eg. 6)
    7 - day (eg. 24)
    time (eg. 3:30:31.123456)
    8 - hour (eg. 3)
    9 - minute (eg. 30)
    10- second (eg. 31)
    11- hundredth of second (eg. 12)
    12- tenthousandth of second (eg. 34)
    13- millionth of second (eg. 56)
    
    INPUT:
      im     - b16 image as 2D numpy array OR filename of b16 image
      scaled - False by default if the input values are 12 bit. 
              True if using the dimax (pixel values are scaled)
    
    OUTPUT:
      timestamp in format - [imnum,day,month,year,hh,mm,ss,fraction of sec]
    """
    if scaled:
        scale = 16
    else:
        scale = 1
    if type(im) is str:
        binary_ts = load_binary_b16_timestamp(im)
    else:
        binary_ts = im[0, :16]
    mil = binary_ts[0] / scale
    tth = binary_ts[1] / scale
    hun = binary_ts[2] / scale
    un  = binary_ts[3] / scale
    imnum = int(int(un / 16) * 10 + un % 16 + \
            int(hun / 16) * 1000 + hun % 16 * 100 + \
            int(tth / 16) * 100000 + tth % 16 * 10000 + \
            int(mil / 16) * 10000000 + mil % 16 * 1000000)           
    
    y1 = binary_ts[4] / scale
    y2 = binary_ts[5] / scale
    year = int(int(y1 / 16) * 1000 + y1 % 16 * 100 + int(y2 / 16) * 10 + y2 % 16)
    
    # Edited 9/2/15 #
    ### None of these were previously scaled and treated at 16bit numbers
    ### Also it never worked properly before this!
    m1 = binary_ts[6] / scale
    month = int(int(m1 / 16) * 10 + m1 % 16)
    d1 = binary_ts[7] / scale
    day = int(int(d1 / 16) * 10 + d1 % 16)
    h1 = binary_ts[8] / scale
    hh = int(int(h1 / 16) * 10 + h1 % 16)
    m1 = binary_ts[9] / scale
    mm = int(int(m1 / 16) * 10 + m1 % 16)
    s1 = binary_ts[10] / scale
    ss = int(int(s1 / 16) * 10 + s1 % 16)
    ###
    
    hth = binary_ts[11] / scale
    tth = binary_ts[12] / scale
    mth = binary_ts[13] / scale
    fs = int(hth / 16) * 0.1 + hth % 16 * 0.01 + \
         int(tth / 16) * 0.001 + tth % 16 * 0.0001 + \
         int(mth / 16) * 0.00001 + mth % 16 * 0.000001 
         
    # Edited 9/2/15 # - commented out the unknowns
#    unknown1 = binary_ts[14]/scale
#    unknown2 = binary_ts[15]/scale
    ts = [imnum, day, month, year, hh, mm, ss, fs]
    return ts
    
def verify_b16_sequence_timestamps(folder='.'):
    fnames = os.listdir(folder)
    time_dict = {}
    #collection
    for fname in fnames:
        if fname.rfind('.') > 0 and fname[fname.rfind('.'):] == '.b16':
            series, num, fext = series_info(fname)
            if series not in time_dict:
                time_dict[series] = []
            timestamp = get_b16_timestamp(fname)
            time_dict[series].append([int(num), timestamp[0], b16_timestamp_to_float(timestamp)])
    #validation
    for key in time_dict:
        print('Data set: {}'.format(key))
        print('Found {} files'.format(len(time_dict[key])))
        ver_data = sorted(time_dict[key])
        for i,e in enumerate(ver_data):
            t = e[2]
            if e[0] != e[1]:
                print('File numbers out of sync: File {} has image number tag {}'.format(e[0], e[1]))
            if i == 0:
                t_prev = t
                continue
            dt = t - t_prev
            if i == 1:
                print('Image spacing detected as {} (frequency: {} based on first two images'.format(dt, 1 / dt))
                t_prev = t
                dt_fix = dt
                continue
            t_prev = t
            if abs(dt - dt_fix) > 2e-6:
                print('Bad time difference in image {} of {}.'.format(e[0], dt))
                
def series_info(fname):
    rindex = fname.rfind('.')
    if rindex == -1:
        fext = ''
        rindex = None
    else:
        fext = r'\{}'.format(fname[rindex:])
    series = re.compile(r'\_[0-9]+{}$'.format(fext))
    rematch = series.search(fname)
    if rematch is None:
        raise ValueError('Could not find a series name of the file {}'.format(fname))
    series_name = fname[: rematch.start()]
    num = fname[rematch.start() + 1: rindex]
    return series_name, num, fext[1:]                        